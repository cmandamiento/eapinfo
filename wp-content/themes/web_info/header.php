<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="es"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="es"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="es"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js" lang="es">           <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="es"> <!--<![endif]-->
    <head profile="http://gmpg.org/xfn/11">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1"/>
        <title><?php wp_title(); ?></title>
        <!-- !LEGACY --> <!--[if lt IE 9]> <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-icon" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
        <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>  
    	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
     <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
          <nav class="nav-collapse collapse">
            <ul class="nav">
              <li<?php active_hold(""); ?>><a href="<?php bloginfo('url'); ?>">Inicio</a></li>
              <li<?php active_hold("aniversario-xviii"); ?>><a href="http://informatica-unjfsc.com/aniversario-xviii/">Cronograma de Actividades</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>    
     <div class="container">
      <div class="row">