<?php get_header();?>
    <?php get_sidebar(); ?>
    <div class="span9">
          <div class="well">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div id="post-<?php the_ID(); $count++; ?>" >
              <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
              <p><?php the_content(); ?></p>
            </div><!--/span-->
            <?php endwhile;endif; ?>
          </div><!--/hero-->
    </div><!--/span-->
<?php get_footer(); ?>