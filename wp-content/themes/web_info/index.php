<?php get_header(); $count_post = 1; ?>
    <div class="span9">
          <section class="hero-unit">
            <h1><?php bloginfo('name'); ?></h1>
            <p><?php bloginfo('description'); ?></p>
            <p>
              <a class="btn btn-info" href="http://informatica-unjfsc.com/aniversario-xviii/">Cronograma de Actividades</a>
            </p>
          </section>
          <!-- BEGIN POSTS -->
            <?php if (have_posts()) : while (have_posts()) : the_post(); 
                  if(($count_post-1)%2==0&&($count_post-1)!=0){echo '</div>';}
                  if(($count_post+1)%2==0||($count_post-1)==0){echo '<div class="row-fluid" style="text-align:justify;">';}
            ?>
            <article class="span6 well item_post" id="post-<?php the_ID(); $count++; ?>" >
            	<figure class="span4" style="text-align:center;">
                  <img src="<?php dp_attachment_image(); ?>" class="img-circle" ?>
              </figure>
              <div class="span6">
                <?php
                  $resumen = get_the_excerpt();
                  $title = get_the_title();
                ?>
              <a title="Ir a <?php the_title(); ?>" href="<?php the_permalink() ?>"><h2 class="item_post_title"><?php echo get_resumen($title,3); ?></h2></a>
              <?php
               echo '<p class="item_post_text">'.get_resumen($resumen,20).'</p>'; ?>
              <p><a title="Continuar leyendo <?php the_title(); ?>" class="btn btn-info" href="<?php the_permalink() ?>">Continuar Leyendo &raquo;</a></p>
              </div>
            </article><!--/span-->
            <?php  $count_post++; endwhile;endif; ?>
          </div><!--/row-->
        </div><!--/span-->
        <?php get_sidebar(); ?>
<?php get_footer(); ?>