<?php

function fix_custom_fields_in_wp342() {
   global $wp_version, $hook_suffix;
 
   if ( '3.4.2' == $wp_version && in_array( $hook_suffix, array( 'post.php', 'post-new.php' ) ) ) : ?>
<script type="text/javascript">
jQuery(document).delegate( '#addmetasub, #updatemeta', 'hover focus', function() {
   jQuery(this).attr('id', 'meta-add-submit');
});
</script>
<?php
   endif;
}

function active_hold($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], "");
    if ($current_file_name == $requestUri)
        echo ' class="active"';
}

add_action( 'admin_print_footer_scripts', 'fix_custom_fields_in_wp342' );



    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div class="well">',
    		'after_widget'  => '</div>',
    		'before_title'  => ' <h4>',
    		'after_title'   => '</h4>'
    	));
    }
	
	//thubnails	
function print_video_thumb_first_post_image($post,&$thumb_url) {
	$content = $post->post_content;
	if (preg_match('/<img[[:alnum:]\s-_=;:"\/]+src="(.*?)"/',$content, $match)) {
		$thumb_url = $match[1];
		return 1;
	}
}
function quitar_barra_administracion(){
    return false;
}
 
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');

function print_video_thumb($post) {
	print_video_thumb_first_post_image($post,$thumb_url);
	echo get_video_thumb(get_permalink($post->ID), $post->post_title,
					$thumb_url);
}
function get_video_thumb($url,$title,$img) {
	return '' .
		$img . '';
}

function get_resumen($excerpt,$nkeywords)
{
    $array_excerpt = explode(' ', $excerpt);

    $retorno = "";
    for($i=0;$i<$nkeywords;$i++){
      $retorno.=$array_excerpt[$i].' ';    
    }
    if($nkeywords<count($array_excerpt))
    {
    $retorno.="...";
    }
    return $retorno;

}

/**
 * Redirecciona los adjuntos al post padre, o bien, a la portada.
 */
function attachment_redirect()
{
	global $post;
	if ( is_attachment() )
    {
		if( $post->post_parent )
            wp_redirect( get_permalink($post->post_parent), 301 );
        else
            wp_redirect( home_url(), 301 );
        exit;
	}
}

function dp_attachment_image($postid=0, $size='thumbnail', $attributes='')
{

 $postid = get_the_ID();
        if ($images = get_children(array(
            'post_parent' => $postid,
            'post_type' => 'attachment',
            'numberposts' => 1,
            'post_mime_type' => 'image',))) {

            foreach ($images as $image) {

                $attachment = wp_get_attachment_image_src($image->ID, $size);
            }
            echo $attachment[0];
        } else {
            global $post, $posts;
            $first_img = '';
            $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
            $first_img = $matches [1] [0];
            if (empty($first_img)) { //Defines a default image
                $img_dir = get_bloginfo('template_directory');
                $first_img = $img_dir . '/images/feat-default.png';
            }
            echo $first_img;
        }
}

add_action( 'template_redirect', 'attachment_redirect', 1 );

?>