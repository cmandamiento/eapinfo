<div class="navigation">
	<?php if(!function_exists('wp_pagenavi')) : ?>
           <div class="next-posts"><?php next_posts_link('&laquo; Entradas Anteriores') ?></div>
	       <div class="prev-posts"><?php previous_posts_link('Entradas Recientes &raquo;') ?></div>
            <?php else : wp_pagenavi(); endif; ?>
</div>