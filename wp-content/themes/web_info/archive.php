<?php get_header(); ?>
   <div class="span9">
    	<?php if (have_posts()) : ?>
 			<?php $post = $posts[0]; 
 			 while (have_posts()) : the_post(); ?>
			<article class="span8 well">
				<figure class="thumbnail span2">
					<img src="<?php dp_attachment_image(); ?>" class="img-circle" ?>
				</figure>
				<div class="span4">
				 <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
	              <?php the_excerpt(); ?>
				<br><br>
				Guardado en: <?php the_category(', ') ?>

				</div>
				<br clear="left" />
			</article>
			<?php endwhile; ?>
			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>		
	<?php else : ?>
		<h2>No se encontro Nada</h2>
	<?php endif; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>