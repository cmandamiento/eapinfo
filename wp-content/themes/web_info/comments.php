<?php
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	if ( post_password_required() ) { ?>
		This post is password protected. Enter the password to view comments.
	<?php
		return;
	}
?>
<?php if ( have_comments() ) : ?>
	
	<div class="alternative" style="margin-top:-15px;"><?php comments_number('0 Comentarios', '1 Comentario', '% Comentarios' );?> en <?php the_title(); ?></div>
	<ol class="commentlist">
		<?php wp_list_comments(array('avatar_size'=>50)); ?>
	</ol>
	<div class="navsgation">
		<div class="next-posts"><?php previous_comments_link() ?></div>
		<div class="prev-posts"><?php next_comments_link() ?></div>
	</div>
<?php else : // this is displayed if there are no comments so far ?>
	<?php if ( comments_open() ) : ?>
	<!-- If comments are open, but there are no comments. -->
	 <?php else : // comments are closed ?>
		<p>Los comentarios estan cerrados.</p>
	<?php endif; ?>
<?php endif; ?>
<?php if ( comments_open() ) : ?>
<div id="respond">
	<div class="com_title"><?php comment_form_title( 'Deja tu Comentario', 'Deja una respuesta para %s' ); ?></div>
	<div class="com_warn">
	  *No Abuses de los Emoticones, no llenes tu comentario de estos.<br />
	  *Tu comentario sera visible despues de ser aprobado.<br />
	  *No seran publicados comentarios que contengan ofensas o SPAM<br />
	  *Quieres que en la imagen de tu comentario salga tu foto o im&aacute;gen? <a href="<?php echo get_option('home'); ?>/imagen-gravatar-comentarios">Le&eacute; este Articulo</a>.
	</div>
	<div class="cancel-comment-reply">
		<?php cancel_comment_reply_link(); ?>
	</div>
	<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
		<p>Necesitas estar <a href="<?php echo wp_login_url( get_permalink() ); ?>">Conectado</a> para escribir un comentario.</p>
	<?php else : ?>
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( is_user_logged_in() ) : ?>
			<p>Conectado como <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Salir de esta cuenta">Salir &raquo;</a></p>
		<?php else : ?>
				<input type="text" name="author" id="author" value="Escribe tu Nombre" onfocus="if(this.value=='Escribe tu Nombre'){this.value=''}"
                   onblur="if(this.value==''){this.value='Escribe tu Nombre'}" />
				<input type="email" name="email" id="email" value="Escribe tu E-mail" onfocus="if(this.value=='Escribe tu E-mail'){this.value='@'}"
                   onblur="if(this.value=='@'){this.value='Escribe tu E-mail'}"  />
		<?php endif; ?>		
		<div class="box_smileys"><span class="com_label">Tu Comentario:</span> <?php include(TEMPLATEPATH . '/smiley.php'); ?></div>
			<textarea name="comment" id="comment"></textarea>
			<input name="submit" type="submit" id="submit" value="Enviar Comentario" />
			<?php comment_id_fields(); ?>
		<?php do_action('comment_form', $post->ID); ?>
	</form>
	<?php endif; // If registration required and not logged in ?>
</div>
<?php endif; ?>