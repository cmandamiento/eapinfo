<?php get_header();?>
    
    <div class="span9">
          <div class="well">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); $count++; ?>" >
              <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
              <a href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;count=horizontal&amp;lang=es" class="twitter-share-button">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
              <iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&layout=button_count&show_faces=false&width=100&action=like&font=verdana&colorscheme=light&height=21" scrolling="no" frameborder="0" style="vertical-align:top;border:none; overflow:hidden; height:20px; width:130px; padding-top:1px;"></iframe>
              <p><?php the_content(); ?></p>
            </article><!--/article-->
            <?php endwhile;endif; ?>
            <div id="comfb" class="fb-comments" data-href="<?php the_permalink(); ?>" data-num-posts="5" data-width="500"></div>
          </div><!--/row-->
        </div><!--/span-->
        <?php get_sidebar(); ?>
<?php get_footer(); ?>