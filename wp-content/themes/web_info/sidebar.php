<div class="span3">
	<aside class="well">
		<h4>Articulos Recientes</h4>
	    <ul>
	   	  <?php $popular = new WP_Query("post_type=post&posts_per_page=5"); while($popular->have_posts()) : $popular->the_post();?>
	    <li class="cat-item">
	        <i class="icon-ok"></i>  <a title="<?php the_title(); ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a>	         
	    </li>
		  <?php endwhile; ?>
	    </ul>
	</aside>
    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>
    <?php endif; ?>
</div><!--/span-->